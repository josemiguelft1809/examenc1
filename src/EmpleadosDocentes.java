/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Figueroa
 */
public class EmpleadosDocentes extends Empleados{
    
    
    private int nivel;
    private float horasLaboradas;
    private float pagoHora;

    public EmpleadosDocentes() {
        this.nivel = 0;
        this.horasLaboradas = 0.f;
        this.pagoHora = 0.0f;
    }
    
    
    
    public EmpleadosDocentes(int nivel, float horasLaboradas, float pagoHora, int numEmpleado, String nombre, String domicilio, Contrato contrato) {
        super(numEmpleado, nombre, domicilio, contrato);
        this.nivel = nivel;
        this.horasLaboradas = horasLaboradas;
        this.pagoHora = pagoHora;
    }

    public int getnivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getHorasLaboradas() {
        return horasLaboradas;
    }

    public void setHorasLaboradas(float horasLaboradas) {
        this.horasLaboradas = horasLaboradas;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }
    
    
    @Override
    public float calcularTotal() {
        return this.pagoHora * this.horasLaboradas;
    }

    @Override
    public float calcularImpuesto() {
        return this.calcularTotal() * (this.contrato.getImpuestoIsr()/100);
    }

    @Override
    public float calcularAdicional() {
        float precio = 0.0f;
        if(this.nivel ==0){
            precio =  ((float) this.calcularTotal() * .35f);
        }
        if(this.nivel ==1){
            precio =  ((float) this.calcularTotal() * .4f);
        }
        if(this.nivel ==2){
            precio =  ((float) this.calcularTotal() * .5f);
        }
        
        return precio;
    }

    @Override
    public float calcularPagoTotal() {
        return this.calcularTotal() - this.calcularImpuesto() + this.calcularAdicional();
    }
}
