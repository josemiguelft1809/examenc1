/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Figueroa
 */
public class Contrato {
    protected int clave;
    protected String puesto;
    protected float impuestoIsr;

    public Contrato() {
        this.clave = 0;
        this.puesto = "";
        this.impuestoIsr = 0.0f;
    }

    public Contrato(int clave, String puesto, float impuestoIsr) {
        this.clave = clave;
        this.puesto = puesto;
        this.impuestoIsr = impuestoIsr;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoIsr() {
        return impuestoIsr;
    }

    public void setImpuestoIsr(float impuestoIsr) {
        this.impuestoIsr = impuestoIsr;
    }
    
    
    
}
